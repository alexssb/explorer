/**
 * Created by alex on 13/10/13.
 */

$(document).ready(function() {
    function recursive_resolution(url){
        var container = $("#box_directories");
        $.ajax({
            url: url,
            dataType: 'json',
            beforeSend: function() {
                container.html('<img src="/static/gif/ajax-loader.gif">')
            }
        }).done(function( data ) {
                var links = "";
                for (var index=0; index < data.paths.length; index++) {
                    var object = data.paths[index];
                    links += '<p>';
                    if (object.error){
                        links += data.message;
                    } else {
                        if (object.is_file){
                            links += '<a href="' + object.endpoint + '">';
                        } else {
                            links += '<a class="endpoint_call" href="' + object.endpoint + '">';
                        }
                        links += object.path + '</a>'
                    }
                    links += '</p>';
                }
                container.html(links);

                var CONTENT_BACK = '<div id="back"><a class="endpoint_call" href="' + data.back + '">BACK</a></div>';
                container.append(CONTENT_BACK).prepend(CONTENT_BACK);

                if (data.root.length > 0){
                    $('#directories').find("option:selected").text(data.root);
                }
                $('.endpoint_call').each(function(){
                    $(this).click(function(event) {
                        event.preventDefault();
                        recursive_resolution(this.href);
                    })
                });
        });
    }
    $("#directories").change(function(){
        recursive_resolution($(this).val());
    });

    var text = $('#directories').find("option:selected").val();
    recursive_resolution(text);
})