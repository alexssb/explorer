#coding: utf-8
import base64
import os
import urllib

__author__ = 'alex'


def join_b64decode(root, path):

    root = base64.b64decode(root)
    path = base64.b64decode(path)

    return os.path.join(root, path)


def join_unquote_plus(root, path):

    root = urllib.unquote_plus(root)
    path = urllib.unquote_plus(path)

    return os.path.join(root, path)