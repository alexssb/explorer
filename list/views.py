from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse

import mimetypes
import json
import os
import time


def setup_endpoints(paths, root=None):
    results = []
    for path in paths:
        params = {'path': path, 'is_file': False}
        if not root is None:
            absolute_path = os.path.join(root, path)
            if os.path.isdir(absolute_path):
                params['endpoint'] = reverse('list.views.resolve_relative_path', args=(root, path))
            elif os.path.isfile(absolute_path):
                params['endpoint'] = reverse('list.views.file_response', args=(absolute_path,))
                params['is_file'] = True
        results.append(params)
    return results


def setup(root, paths):
    if len(root) > 0:
        back = reverse('list.views.resolve_absolute_path', args=(os.path.dirname(root),))
    else:
        back = ""
    return {'root': root, 'back': back, 'paths': paths}


def indexes(request):
    temp = []
    default_dirs = [os.path.abspath(path) for key, path in os.environ.items() if os.path.isdir(path)]
    def not_in(item):
        if item not in temp:
            temp.append(item)
            return True
        return False
    default_dirs = filter(not_in, default_dirs)
    default_dirs.sort()
    default_dirs = setup_endpoints(default_dirs)
    return render_to_response('index.html', {'default_dirs': default_dirs}, mimetype='text/html')


def resolve_absolute_path(request, path):
    return resolve_relative_path(request, '', path)


def resolve_relative_path(request, root, path):
    path = os.path.join(root, path)
    if os.path.isdir(path):
        try:
            result = setup(path, setup_endpoints(os.listdir(path), root=path))
        except os.error as err:
            message = "The directory '%s' can not be read" % path
            result = setup(path, [{'error': True, 'message': message}])
    else:
        message = "File or directory not found"
        result = setup(path, [{'error': True, 'message': message}])
    time.sleep(0.2)
    return HttpResponse(json.dumps(result), mimetype='application/json')


def file_response(request, path):
    _type, encoding = mimetypes.guess_type(path)
    try:
        filename = os.path.basename(path)
        response = HttpResponse(open(path, 'rb'), mimetype=_type)
        response['Content-Disposition'] = 'attachment; filename="{name}"'.format(name=filename)
    except os.error as err:
        response = HttpResponse("The file '%s' can not be read" % path, mimetype='text/plain')
    return response


def resolve_relative_file_path(request, root, path):
    path = os.path.join(root, path)
    return file_response(request, path)


def resolve_absolute_file_path(request, path):
    return file_response(request, path)
