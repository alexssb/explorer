from django.conf.urls import patterns, include, url

__author__ = 'alex'

urlpatterns = patterns('',
    url(r'^index/$', 'list.views.indexes'),
    url(r'^resolve-rel/(?P<root>.+)/(?P<path>.+)/$', 'list.views.resolve_relative_path'),
    url(r'^resolve-abs/(?P<path>.+)/$', 'list.views.resolve_absolute_path'),
    url(r'^resolve-file/(?P<path>.+)/$', 'list.views.file_response')
)
